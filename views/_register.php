<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Matteo Bruni">
    <title>Register Template - tsParticles</title>

    <!-- Custom styles for this template -->
    <link href="css/particles.css" rel="stylesheet" />
    <link href="css/auth.css" rel="stylesheet" />
</head>

<body>
    <div id="tsparticles"></div>
    <main class="box">
        <h2>Register</h2>
        <form method="POST">
            <div class="inputBox">
                <label for="userName">Username</label>
                <input type="text" name="username" id="username" value='<?= isset($_SESSION['res']) ? $_SESSION['res']['username'] : '' ?>' placeholder="type your username" required />
            </div>
            <div class="inputBox">
                <label for="userPassword">Password</label>
                <input type="password" name="password" id="password" placeholder="type your password" required />
            </div>
            <div class="inputBox">
                <label for="userConfirmPassword">Confirm Password</label>
                <input type="password" name="confirmPassword" id="confirmPassword" placeholder="confirm your password" required />
            </div>
            <div class="inputBox">
                <label for="userConfirmPassword">Email</label>
                <input type="email" name="email" id="email" value='<?= isset($_SESSION['res']) ? $_SESSION['res']['email'] : '' ?>' placeholder="type your email" required />
            </div>
            <button type="submit" name="addUser" style="float: left;">Submit</button>
            <a class="button" href="index.php" style="float: left;">Login</a>
            <a class="button" href="reset.php" style="float: left;">Forgon Pass</a>
        </form>
        <p style='color: purple'><?= isset($_SESSION['message']) ? $_SESSION['message'] : '' ?></p>
        <?php 
            if (isset($_SESSION['message']))
            {
                unset($_SESSION['message']);
            }
        ?>
    </main>
    <footer>
    </footer>

    <script src="https://cdn.jsdelivr.net/npm/tsparticles@1.17.5/dist/tsparticles.min.js"
        integrity="sha256-Wcr5q//r5yGozjRl+ToXruCoy75vgiApkKsV30aCns8=" crossorigin="anonymous"></script>
    <script type="text/javascript" src="js/particles.js"></script>
</body>

</html>

<?php 
if (isset($_SESSION['res']))
{
    unset($_SESSION['res']); 
}
?>