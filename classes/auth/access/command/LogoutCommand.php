<?php
namespace auth\access\command;
use \auth\registry\Registry;

class LogoutCommand extends Command
{
    public function execute(CommandContext $context): bool
    {
        $reg = Registry::instance();
        $manager = $reg->getAccessManager();
        $email = $context->get('email');

        $inputs = [
            'email' => $email
        ];

        $action = $manager->action('logout', $inputs);

        if (!$action)
        {
            $context->setMsg($manager->getMsg());
            return false;
        }

        $context->setMsg($manager->getMsg());
        return true;
    }
}