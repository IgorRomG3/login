<?php
namespace connection;

class DBConnect {
  private $link;

  public function __construct()
  {
    $config = DBConfig::getInstance();
    $this->link = new \mysqli(
      $config->getHost(),
      $config->getUser(),
      $config->getPass(),
      $config->getName()
    );

    if ($this->link->connect_errno) 
    {
      throw new \Exception("Не удалось подключиться к MySQL: (" . $this->link->connect_errno . ") " . $this->link->connect_error);
    }
  }

  public function __destruct()
  {
    $this->link->close();
  }

  public function query($query)
  {
    return $this->link->query($query);
  }

  public function update($query)
  {
    $result = $this->query($query);

    if ($this->link->affected_rows == 1)
    {
      return true;
    } 

    return false;
  }

  public function fetch_one($query)
  {
    $result = $this->query($query);

    return $result->fetch_assoc();
  }

  public function fetch_array($query): array
  {
    $rows = [];
    $result = $this->query($query);

    while($row = $result->fetch_assoc())
    {
      $rows[] = $row;
    }

    return $rows;
  }

  public function real_escape_string($value): string
  {
    return $this->link->real_escape_string($value);
  }
}