<?php
namespace auth\input;

abstract class Input {
  protected $input;

  public function __construct($input)
  {
    $this->input = $input;
  }

  abstract public function getInput();
}