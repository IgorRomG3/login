<?php
namespace auth\pagecontrollers;
use core\PageController;
use \auth\access\command\CommandContext;
use \auth\access\command\LogoutCommand;

abstract class UserPageController extends PageController
{
    public function process()
    {
        if (!$this->reg->isLogedIn())
        {
            $this->forward('index.php');
            exit;
        }

        if (isset($_POST['exit']) || (isset($_SESSION['LAST_ACTIVITY']) && (time() - $_SESSION['LAST_ACTIVITY'] > 60)))
        {
            $context = new CommandContext();
            $logout = new LogoutCommand();
            $res = $logout->execute($context);

            if ($res)
            {
                unset($_SESSION['LAST_ACTIVITY']);
                $this->forward('admin.php');
                exit;
            } else {
                echo $context->getMsg();
                exit;
            }
        }
        $_SESSION['LAST_ACTIVITY'] = time();
        
        $this->doProcess();
    }

    abstract protected function doProcess();
}