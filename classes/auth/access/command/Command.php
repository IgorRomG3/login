<?php
namespace auth\access\command;
use \connection\DBConnect;

abstract class Command
{
    protected $db;
    protected static $table = 'users';

    public function __construct()
    {
        $this->db = new DBConnect();
    }

    abstract public function execute(CommandContext $context): bool;
}