<?php
namespace auth\access\command;
use \auth\registry\Registry;

class RegisterCommand extends Command
{
    public function execute(CommandContext $context): bool
    {
        $reg = Registry::instance();
        $manager = $reg->getAccessManager();
        $username = $context->get('username');
        $password = $context->get('password');
        $confirmPassword = $context->get('confirmPassword');
        $email = $context->get('email');
        $inputs = [
            'username' => $username,
            'password' => $password,
            'confirmPassword' => $confirmPassword,
            'email' => $email,
        ];
        $action = $manager->action('register', $inputs);

        if (!$action)
        {
            $context->setMsg($manager->getMsg());
            return false;
        }

        $context->setMsg($manager->getMsg());
        return true;
    }
}