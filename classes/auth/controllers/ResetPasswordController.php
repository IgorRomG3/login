<?php
declare(strict_types = 1);

namespace auth\controllers;
use \auth\pagecontrollers\GuestPageController;
use \auth\access\command\CommandContext;
use \auth\access\command\ResetPasswordCommand;

class ResetPasswordController extends GuestPageController
{
    public function doProcess()
    {
        if (isset($_POST['reset']))
        {
            $context = new CommandContext();
            $resetPassword = new ResetPasswordCommand();
            $res = $resetPassword->execute($context);

            if ($res)
            {
                $_SESSION['message'] = $context->getMsg();
            }
            else {
                $_SESSION['email'] = $_POST['email'];
                $_SESSION['message'] = $context->getMsg();
            }
        }

        $this->render($_SERVER['DOCUMENT_ROOT'] . '/views/_reset.php', $this->request);
    }
}
