<?php
namespace auth\input;

class TextInput extends Input
{
  public function getInput()
  {
    return strip_tags(trim($this->input));
  }
}