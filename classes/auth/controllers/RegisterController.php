<?php
declare(strict_types = 1);

namespace auth\controllers;
use \auth\pagecontrollers\GuestPageController;
use \auth\access\command\CommandContext;
use \auth\access\command\RegisterCommand;

class RegisterController extends GuestPageController
{
    public function doProcess()
    {
        if (isset($_POST['addUser']))
        {
            $context = new CommandContext();
            $register = new RegisterCommand();
            $res = $register->execute($context);
            
            if (!$res)
            {
                $_SESSION['res']['username'] = $_POST['username'];
                $_SESSION['res']['email'] = $_POST['email'];
                $_SESSION['message'] = $context->getMsg();
            }
            else 
            {
                $_SESSION['message'] = $context->getMsg();
            }
        }

        $this->render($_SERVER['DOCUMENT_ROOT'] . '/views/_register.php', $this->request);
    }
}
