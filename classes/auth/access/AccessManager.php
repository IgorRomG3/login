<?php
namespace auth\access;

class AccessManager
{
    private $msg = '';

    public function getMsg()
    {
        return $this->msg;
    }

    private function setMsg($msg)
    {
        return $this->msg = $msg;
    }

    public function action($type, $inputs)
    {
        $type = '\\auth\\access\\' . str_replace(' ', '', ucwords(str_replace('-', ' ', $type))) . 'User';
        
        if (!class_exists($type))
        {
            throw new \Exception("Класса {$type} не существует в папке auth");
        }

        $action = new $type($inputs);
        $result = $action->run();
        $msg = $action->getMsg();

        $this->setMsg($msg);

        return $result;
    }
}