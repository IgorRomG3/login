<?php
use auth\controllers\ResetPasswordController;

session_start();
require_once './vendor/autoload.php';

$reset = new ResetPasswordController();
$reset->init();
$reset->process();