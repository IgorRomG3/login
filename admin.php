<?php

use auth\controllers\AdminController;

session_start();
require_once './vendor/autoload.php';

$admin = new AdminController();
$admin->init();
$admin->process();