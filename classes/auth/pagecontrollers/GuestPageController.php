<?php
namespace auth\pagecontrollers;
use \core\PageController;

abstract class GuestPageController extends PageController
{
    public function process()
    {
        if ($this->reg->isLogedIn())
        {
            $this->forward('admin.php');
            exit;
        }

        $this->doProcess();
    }

    abstract protected function doProcess();
}