<?php
namespace auth\access;

use \connection\DBConnect;

abstract class User {
    protected $db;
    protected const table = 'users';
    protected $inputs;
    protected $msg = '';

    public function __construct($inputs)
    {
        $this->inputs = $inputs;
        $this->db = new DBConnect();
    }

    public function getMsg()
    {
        return $this->msg;
    }

    protected function setMsg($msg)
    {
        $this->msg = $msg;
    }

    abstract public function run();
}