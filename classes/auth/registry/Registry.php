<?php
namespace auth\registry;

use auth\access\AccessManager;
use core\Request;

class Registry
{
    private static $instance = null;
    private $request = null;

    public static function instance(): self
    {
        if (is_null(self::$instance))
        {
            self::$instance = new self();
        }

        return self::$instance;
    }

    public function isLogedIn()
    {   
        if (isset($_SESSION['SESS']) && isset($_SESSION['LOGIN']) && isset($_SESSION['PASS']))
        {
            return true;
        } 
        else if (isset($_COOKIE['login']['email']) && isset($_COOKIE['login']['password']))
        {
            $inputs = [
                'email' => $_COOKIE['login']['email'],
                'password' => $_COOKIE['login']['password'],
                'remember' => false
            ];

            return $this->getAccessManager()->action('login', $inputs);
        }
        
        return false;
    }

    public function getAccessManager()
    {
        return new AccessManager();
    }

    // must be initialized by some smarter component
    public function setRequest(Request $request)
    {
        $this->request = $request;
    }

    public function getRequest(): Request
    {
        if (is_null($this->request)) {
            throw new \Exception("Запрос не задан");
        }

        return $this->request;
    }
}