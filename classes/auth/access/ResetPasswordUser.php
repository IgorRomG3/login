<?php
namespace auth\access;

use \auth\input\TextInput;
use \auth\input\PasswordInput;
use \auth\input\ConfirmPasswordInput;

class ResetPasswordUser extends User
{
    public function run()
    {
        $email = new TextInput($this->inputs['email']);
        $confirmPassword = new ConfirmPasswordInput(new PasswordInput($this->inputs['password']), new PasswordInput($this->inputs['confirmPassword']));
        $emailInput = $email->getInput();
        $passwordInput = $confirmPassword->getInput();

        if (!$passwordInput)
        {
            $this->setMsg('Пароли не совпадают');
            return false;
        }

        $changePasswordQuery = "UPDATE " . self::table . " SET password='%s' WHERE email='%s'";
        $changePasswordQuery = sprintf($changePasswordQuery, $passwordInput, $emailInput);
        $changePassword = $this->db->update($changePasswordQuery);

        if (!$changePassword)
        {
            $this->setMsg("Пользователя с эмейлом $emailInput не существует или данный пароль уже существует");
            return false;
        }
        else
        {
            $this->setMsg('Пароль успешно изменен');
            return true;
        }
    }
}