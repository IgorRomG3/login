<?php
declare(strict_types = 1);

namespace core;

use \auth\registry\Registry;

abstract class PageController
{
    protected $reg;
    protected $request;

    abstract public function process();

    public function __construct()
    {
        $this->reg = Registry::instance();
    }

    public function init()
    {
        $request = new HttpRequest();

        $this->reg->setRequest($request);
        $this->request = $this->reg->getRequest();
    }

    public function forward(string $resource)
    {
        $request = $this->getRequest();
        $request->forward($resource);
    }

    public function render(string $resource, Request $request)
    {
        include($resource);
    }

    public function getRequest()
    {
        return $this->reg->getRequest();
    }
}
