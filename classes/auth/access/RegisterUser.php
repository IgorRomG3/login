<?php
namespace auth\access;

use \auth\input\TextInput;
use \auth\input\PasswordInput;
use \auth\input\ConfirmPasswordInput;

class RegisterUser extends User
{
    public function run()
    {
        $username = new TextInput($this->inputs['username']);
        $password = new ConfirmPasswordInput(new PasswordInput($this->inputs['password']), new PasswordInput($this->inputs['confirmPassword']));
        $email = new TextInput($this->inputs['email']);
        $hash = md5(microtime());
        $usernameInput = $username->getInput();
        $passwordInput = $password->getInput();
        $emailInput = $email->getInput();

        if (!$passwordInput)
        {
            $this->setMsg('Пароли не совпадают');
            return false;
        }

        $isExistQuery = "SELECT * FROM " . self::table . " WHERE email='$emailInput'";
        $isExist = $this->db->query($isExistQuery);

        if ($isExist->num_rows > 0)
        {
            $this->setMsg('Данный пользователь уже существует');
            return false;
        }

        $registerQuery = "INSERT INTO " . self::table . " (name, password, email, hash, active) VALUES ('%s', '%s', '%s', '%s', '1')";
        $registerQuery = sprintf($registerQuery, $usernameInput, $passwordInput, $emailInput, $hash);
 
        if ($this->db->query($registerQuery))
        {
          $this->setMsg('Вы успешно зарегистрированны в системе');
          return true;
        } else {
          $this->setMsg('Ошибка при регистрации');
          return false;
        }
    }
}