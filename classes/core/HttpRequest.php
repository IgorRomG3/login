<?php
declare(strict_types = 1);

namespace core;

class HttpRequest extends Request
{
    public function init()
    {
        $this->properties = $_REQUEST;
        if (isset($_SERVER['PATH_INFO']))
        {
            $this->path = $_SERVER['PATH_INFO'];
        }
        $this->path = (empty($this->path)) ? "/" : $this->path;
    }

    public function forward(string $path)
    {
        header("Location: {$path}");
        exit;
    }
}
