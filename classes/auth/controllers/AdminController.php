<?php
declare(strict_types = 1);

namespace auth\controllers;
use \auth\pagecontrollers\UserPageController;

class AdminController extends UserPageController
{
    public function doProcess()
    {
        $this->render($_SERVER['DOCUMENT_ROOT'] . '/views/_admin.php', $this->request);
    }
}
