<?php
namespace connection;

class DBConfig {
  private static $instance;
  private static $local = 1;
  private $connection;

  protected function __construct()
  {
    switch(self::$local) {
      case 0:
        $this->connection = [
          'host' => 'outerhost',
          'dbuser' => 'outeruser',
          'dbpass' => 'outerpass',
          'dbname' => 'outername'
        ];
        break;
      default: 
        $this->connection = [
          'host' => 'localhost',
          'dbuser' => 'root',
          'dbpass' => 'root',
          'dbname' => 'login'
        ];
        break;
    }
  }

  protected function __clone()
  {
  }

  public function __wakeup()
  {
    throw new \Exception("Нельзя сериализовать singleton-обьект");
  }

  public static function getInstance()
  {
    if (!isset(self::$instance)) 
    {
        self::$instance = new static();
    }
    return self::$instance;
  }

  public function getHost()
  {
    return $this->connection['host'];
  }

  public function getUser()
  {
    return $this->connection['dbuser'];
  }

  public function getPass()
  {
    return $this->connection['dbpass'];
  }
  public function getName()
  {
    return $this->connection['dbname'];
  }
}