<?php
namespace auth\access\command;
use \auth\registry\Registry;

class ResetPasswordCommand extends Command
{
    public function execute(CommandContext $context): bool
    {
        $reg = Registry::instance();
        $manager = $reg->getAccessManager();
        $email = $context->get('email');
        $password = $context->get('resetPassword');
        $confirmPassword = $context->get('confirmResetPassword');
        $inputs = [
            'email' => $email,
            'password' => $password,
            'confirmPassword' => $confirmPassword,
        ];
        $action = $manager->action('reset-password', $inputs);

        if (!$action)
        {
            $context->setMsg($manager->getMsg());
            return false;
        }

        $context->setMsg($manager->getMsg());
        return true;
    }
}