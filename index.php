<?php
use auth\controllers\LoginController;

session_start();
require_once './vendor/autoload.php';

$login = new LoginController();
$login->init();
$login->process();