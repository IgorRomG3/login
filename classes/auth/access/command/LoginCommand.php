<?php
namespace auth\access\command;
use \auth\registry\Registry;

class LoginCommand extends Command
{
    public function execute(CommandContext $context): bool
    {
        $reg = Registry::instance();
        $manager = $reg->getAccessManager();
        $email = $context->get('email');
        $password = $context->get('userpassword');
        $remember = false;

        if ($context->get('remember') !== null)
        {
            $remember = true;
        }

        $inputs = [
            'email' => $email,
            'password' => $password,
            'remember' => $remember
        ];

        $action = $manager->action('login', $inputs);

        if (!$action)
        {
            $context->setMsg($manager->getMsg());
            return false;
        }

        $context->setMsg($manager->getMsg());
        return true;
    }
}