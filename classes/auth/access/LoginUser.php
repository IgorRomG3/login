<?php
namespace auth\access;

use \auth\input\TextInput;
use \auth\input\PasswordInput;

class LoginUser extends User
{
    public function run()
    {
        $email = new TextInput($this->inputs['email']);
        $password = new PasswordInput($this->inputs['password']);
        $sess = md5(microtime());
        $emailInput = $email->getInput();
        $passwordInput = $password->getInput();

        $loginQuery = "SELECT * FROM " . self::table . " WHERE email='%s' AND password='%s'";
        $loginQuery = sprintf($loginQuery, $emailInput, $passwordInput);
        $login = $this->db->fetch_one($loginQuery);

        if ($login) 
        {
            $this->setMsg("Пользователь {$emailInput} авторизировался успешно");
            $_SESSION['SESS'] = $sess;
            $_SESSION['LOGIN'] = $emailInput;
            $_SESSION['PASS'] = $passwordInput;

            if ($this->inputs['remember'])
            {
                setcookie('login[email]', $emailInput, time() + 3600);
                setcookie('login[password]', $passwordInput, time() + 3600);
            }

            return true;
        }

        $this->setMsg("Неправильный логин или пароль");
        return false;
    }
}