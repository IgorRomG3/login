<?php
use auth\controllers\RegisterController;

session_start();
require_once './vendor/autoload.php';

$register = new RegisterController();
$register->init();
$register->process();