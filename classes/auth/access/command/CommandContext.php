<?php
namespace auth\access\command;

class CommandContext
{
    private $params = [];
    private $msg = "";

    public function __construct()
    {
        $this->params = $_REQUEST;
    }

    public function addParam($key, $value)
    {
        $this->params[$key] = $value;
    }

    public function get($key)
    {
        if (isset($this->params[$key]))
        {
            return $this->params[$key];
        }

        return null;
    }

    public function setMsg(string $msg): string
    {
        return $this->msg = $msg;
    }

    public function getMsg()
    {
        return $this->msg;
    }
}