<?php
declare(strict_types = 1);

namespace auth\controllers;
use \auth\pagecontrollers\GuestPageController;
use \auth\access\command\CommandContext;
use \auth\access\command\LoginCommand;

class LoginController extends GuestPageController
{
    public function doProcess()
    {
        if (isset($_POST['login']))
        {
            $context = new CommandContext();
            $login = new LoginCommand();
            $res = $login->execute($context);

            if ($res == true)
            {
                $this->forward('admin.php');
                exit;
            } else {
                $_SESSION['msg'] = $context->getMsg();
            }
        }

        $this->render($_SERVER['DOCUMENT_ROOT'] . '/views/_login.php', $this->request);
    }
}
