<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Matteo Bruni">
    <title>Signin Template - tsParticles</title>

    <!-- Custom styles for this template -->
    <link href="css/particles.css" rel="stylesheet" />
    <link href="css/auth.css" rel="stylesheet" />
</head>

<body>
    <div id="tsparticles"></div>
    <main class="box">
        <h2>Login</h2>
        <form method='POST'>
            <div class="inputBox">
                <label for="userName">Email</label>
                <input type="email" name="email" id="email" placeholder="type your email" required />
            </div>
            <div class="inputBox">
                <label for="userPassword">Password</label>
                <input type="password" name="userpassword" id="userpassword" placeholder="type your password"
                    required />
            </div>
            <div class="inputBox"> 
                <input type="checkbox" name="remember" id="remember" value='1'>
                <label for="remember">Remember Me</label>
            </div>
            <div>
                <button type="submit" name="login" style="float: left;">Submit</button>
                <a class="button" href="register.php" style="float: left;">Register</a>
                <a class="button" href="reset.php" style="float: left;">Forgon Pass</a>
            </div>
        </form>
       <?php if (isset($_SESSION['msg'])): ?>
        <p><?= $_SESSION['msg'] ?></p>
       <?php endif; ?>
    </main>
    <footer>
    </footer>

    <script src="https://cdn.jsdelivr.net/npm/tsparticles@1.17.5/dist/tsparticles.min.js"
        integrity="sha256-Wcr5q//r5yGozjRl+ToXruCoy75vgiApkKsV30aCns8=" crossorigin="anonymous"></script>
    <script type="text/javascript" src="js/particles.js"></script>
</body>

</html>
<?php
if (isset($_SESSION['msg']))
{
    unset($_SESSION['msg']);
}
?>