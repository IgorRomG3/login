<?php
namespace auth\access;

class LogoutUser extends User
{
    public function run()
    {
        session_destroy();
        setcookie('login[email]', null, -1);
        setcookie('login[password]', null, -1);
        $this->setMsg('Выход произошел успешно');
        return true;
    }
}