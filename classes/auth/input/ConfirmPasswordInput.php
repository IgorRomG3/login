<?php
namespace auth\input;

class ConfirmPasswordInput extends Input
{
  protected $confirm;

  public function __construct(PasswordInput $input, PasswordInput $confirm)
  {
    $this->input = $input;
    $this->confirm = $confirm;
  }
  public function getInput()
  {
    $password = $this->input->getInput();
    $confirm = $this->confirm->getInput();

    if ($password == $confirm)
    {
      return $password;
    }
    else 
    {
      return false;
    }
  }
}